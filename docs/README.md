# Requirements

- Download [this project](https://gitlab.com/dockerized-apps/proof-of-concept).
- Pick up and install the [Docker CE](https://docs.docker.com/install/linux/docker-ce/ubuntu/).
- Follow [guidance here](https://docs.docker.com/compose/install/) to install docker-compose.

## Get up and running

Once you have all requirements, navigate to the project path and run following commands:

- npm install

- docker-compose up --build or docker-compose up --build

- http://localhost:3000

- npx oasgraph http://localhost:3000/openapi.json

##### Notice that users data cannot be manipulated because of authentication. If you used 'admin' as user name and password you can get the data you expected, by running the following command

- curl -u admin:admin http://localhost:3000/users
