/* Basic data shape */
DROP DATABASE IF EXISTS `loopBackDatabase`;
CREATE DATABASE loopBackDatabase;
USE loopBackDatabase;

CREATE TABLE IF NOT EXISTS users
(
  id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
  first_name VARCHAR (25) NOT NULL,
  last_name VARCHAR (25),
  email VARCHAR (35) NOT NULL,
  phone VARCHAR (15)
);

CREATE TABLE IF NOT EXISTS courses
(
  id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
  name VARCHAR (50) NOT NULL,
  type VARCHAR (25) NOT NULL,
  description VARCHAR (150),
  owner VARCHAR (25)
);

CREATE TABLE IF NOT EXISTS modules
(
  id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
  name VARCHAR (50) NOT NULL,
  description VARCHAR (150),
  hours SMALLINT
);

CREATE TABLE IF NOT EXISTS course_modules
(
  id INT NOT NULL AUTO_INCREMENT,
  course_id INT NOT NULL,
  module_id INT NOT NULL,
  PRIMARY KEY (id, course_id, module_id)
);

/* Basic data values */
INSERT INTO users
  (first_name, last_name, email, phone)
VALUES
  ('User 1', 'User 1', 'User_1@test.com', '123 45 678'),
  ('User 2', 'User 2', 'User_2@test.com', '876 54 321');

INSERT INTO courses
  (name, type, description, owner)
VALUES
  ('Course 1', 'Test', 'Testing Courses', 'Owner_1@test.com'),
  ('Course 2', 'Test', 'Testing Courses', 'Owner_2@test.com');

INSERT INTO modules
  (name, description, hours)
VALUES
  ('Moudle 1', 'Testing Moudles', 150),
  ('Moudle 2', 'Testing Moudles', 250),
  ('Moudle 3', 'Testing Moudles', 350),
  ('Moudle 4', 'Testing Moudles', 450);

INSERT INTO course_modules
  (course_id, module_id)
VALUES
  (1, 1),
  (1, 2),
  (2, 3),
  (2, 4);
