import { ProofOfConceptApplication } from './application';
import { ApplicationConfig } from '@loopback/core';

export { ProofOfConceptApplication };

export async function main(options: ApplicationConfig = {}) {
  const app = new ProofOfConceptApplication(options);
  await app.boot();
  await app.start();

  const url = app.restServer.url;
  console.log(`Server is running at ${url}`);

  return app;
}
