import { inject } from '@loopback/core';
import { juggler } from '@loopback/repository';
import * as config from './mysql-datasource.datasource.json';

export class MysqlDatasourceDataSource extends juggler.DataSource {
  static dataSourceName = 'mysqlDatasource';

  constructor(
    @inject('datasources.config.mysqlDatasource', { optional: true })
    dsConfig: object = config,
  ) {
    super(dsConfig);
  }
}
