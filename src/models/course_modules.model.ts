import { Entity, model, property } from '@loopback/repository';

@model()
export class course_modules extends Entity {
  @property({
    type: 'number',
    id: true,
  })
  id?: number;

  @property({
    type: 'number',
    required: true,
  })
  course_id: number;

  @property({
    type: 'number',
    required: true,
  })
  module_id: number;


  constructor(data?: Partial<course_modules>) {
    super(data);
  }
}
