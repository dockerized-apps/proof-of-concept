import { Entity, model, property } from '@loopback/repository';

@model()
export class modules extends Entity {
  @property({
    type: 'number',
    id: true,
  })
  id?: number;

  @property({
    type: 'string',
    required: true,
  })
  name: string;

  @property({
    type: 'string',
  })
  description?: string;

  @property({
    type: 'number',
    required: true,
  })
  hours: number;


  constructor(data?: Partial<modules>) {
    super(data);
  }
}
