import { Entity, model, property } from '@loopback/repository';

@model()
export class courses extends Entity {
  @property({
    type: 'number',
    id: true,
  })
  id?: number;

  @property({
    type: 'string',
    required: true,
  })
  name: string;

  @property({
    type: 'string',
    required: true,
  })
  type: string;

  @property({
    type: 'string',
  })
  description?: string;

  @property({
    type: 'string',
  })
  owner?: string;


  constructor(data?: Partial<courses>) {
    super(data);
  }
}
