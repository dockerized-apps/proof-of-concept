import {Strategy} from 'passport';
import {BasicStrategy} from 'passport-http';
import {Provider, inject, ValueOrPromise} from '@loopback/context';
import {
  UserProfile,
  AuthenticationBindings,
  AuthenticationMetadata,
} from '@loopback/authentication';

export class AuthStrategyProvider implements Provider<Strategy | undefined> {
  constructor(
    @inject(AuthenticationBindings.METADATA)
    private metadata: AuthenticationMetadata,
  ) {}

  value(): ValueOrPromise<Strategy | undefined> {
    if (!this.metadata) {
      return undefined;
    }

    const name = this.metadata.strategy;
    if (name === 'BasicStrategy') {
      return new BasicStrategy(this.verify);
    } else {
      return Promise.reject(`The strategy ${name} is not available.`);
    }
  }

  verify(
    username: string,
    password: string,
    cb: (err: Error | null, user?: UserProfile | false) => void,
  ) {
    // Basic authentication mechanism.
    if (username === 'admin' && password === 'admin') {
      cb(null, {id: '1'});
    } else {
      cb(null, false);
    }
  }
}
