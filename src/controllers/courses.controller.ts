import {
  Count,
  CountSchema,
  Filter,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getFilterSchemaFor,
  getWhereSchemaFor,
  patch,
  put,
  del,
  requestBody,
} from '@loopback/rest';
import { courses } from '../models';
import { CoursesRepository } from '../repositories';

export class CoursesController {
  constructor(
    @repository(CoursesRepository)
    public coursesRepository: CoursesRepository,
  ) { }

  @post('/courses', {
    responses: {
      '200': {
        description: 'Courses model instance',
        content: { 'application/json': { schema: { 'x-ts-type': courses } } },
      },
    },
  })
  async create(@requestBody() courses: courses): Promise<courses> {
    return await this.coursesRepository.create(courses);
  }

  @get('/courses/count', {
    responses: {
      '200': {
        description: 'Courses model count',
        content: { 'application/json': { schema: CountSchema } },
      },
    },
  })
  async count(
    @param.query.object('where', getWhereSchemaFor(courses)) where?: Where,
  ): Promise<Count> {
    return await this.coursesRepository.count(where);
  }

  @get('/courses', {
    responses: {
      '200': {
        description: 'Array of Courses model instances',
        content: {
          'application/json': {
            schema: { type: 'array', items: { 'x-ts-type': courses } },
          },
        },
      },
    },
  })
  async find(
    @param.query.object('filter', getFilterSchemaFor(courses)) filter?: Filter,
  ): Promise<courses[]> {
    return await this.coursesRepository.find(filter);
  }

  @patch('/courses', {
    responses: {
      '200': {
        description: 'Courses PATCH success count',
        content: { 'application/json': { schema: CountSchema } },
      },
    },
  })
  async updateAll(
    @requestBody() courses: courses,
    @param.query.object('where', getWhereSchemaFor(courses)) where?: Where,
  ): Promise<Count> {
    return await this.coursesRepository.updateAll(courses, where);
  }

  @get('/courses/{id}', {
    responses: {
      '200': {
        description: 'Courses model instance',
        content: { 'application/json': { schema: { 'x-ts-type': courses } } },
      },
    },
  })
  async findById(@param.path.number('id') id: number): Promise<courses> {
    return await this.coursesRepository.findById(id);
  }

  @patch('/courses/{id}', {
    responses: {
      '204': {
        description: 'Courses PATCH success',
      },
    },
  })
  async updateById(
    @param.path.number('id') id: number,
    @requestBody() courses: courses,
  ): Promise<void> {
    await this.coursesRepository.updateById(id, courses);
  }

  @put('/courses/{id}', {
    responses: {
      '204': {
        description: 'Courses PUT success',
      },
    },
  })
  async replaceById(
    @param.path.number('id') id: number,
    @requestBody() courses: courses,
  ): Promise<void> {
    await this.coursesRepository.replaceById(id, courses);
  }

  @del('/courses/{id}', {
    responses: {
      '204': {
        description: 'Courses DELETE success',
      },
    },
  })
  async deleteById(@param.path.number('id') id: number): Promise<void> {
    await this.coursesRepository.deleteById(id);
  }
}
