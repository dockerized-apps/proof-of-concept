import {
  Count,
  CountSchema,
  Filter,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getFilterSchemaFor,
  getWhereSchemaFor,
  patch,
  put,
  del,
  requestBody,
} from '@loopback/rest';
import {users} from '../models';
import {UsersRepository} from '../repositories';
import {authenticate} from '@loopback/authentication';

/**
 * Decorating every methods with @authenticate to require the request to be authenticated.
 */
export class UsersController {
  constructor(
    @repository(UsersRepository)
    public usersRepository: UsersRepository,
  ) {}

  @authenticate('BasicStrategy')
  @post('/users', {
    responses: {
      '200': {
        description: 'Users model instance',
        content: {'application/json': {schema: {'x-ts-type': users}}},
      },
    },
  })
  async create(@requestBody() users: users): Promise<users> {
    return await this.usersRepository.create(users);
  }

  @authenticate('BasicStrategy')
  @get('/users/count', {
    responses: {
      '200': {
        description: 'Users model count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async count(
    @param.query.object('where', getWhereSchemaFor(users)) where?: Where,
  ): Promise<Count> {
    return await this.usersRepository.count(where);
  }

  @authenticate('BasicStrategy')
  @get('/users', {
    responses: {
      '200': {
        description: 'Array of Users model instances',
        content: {
          'application/json': {
            schema: {type: 'array', items: {'x-ts-type': users}},
          },
        },
      },
    },
  })
  async find(
    @param.query.object('filter', getFilterSchemaFor(users)) filter?: Filter,
  ): Promise<users[]> {
    return await this.usersRepository.find(filter);
  }

  @authenticate('BasicStrategy')
  @patch('/users', {
    responses: {
      '200': {
        description: 'Users PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async updateAll(
    @requestBody() users: users,
    @param.query.object('where', getWhereSchemaFor(users)) where?: Where,
  ): Promise<Count> {
    return await this.usersRepository.updateAll(users, where);
  }

  @authenticate('BasicStrategy')
  @get('/users/{id}', {
    responses: {
      '200': {
        description: 'Users model instance',
        content: {'application/json': {schema: {'x-ts-type': users}}},
      },
    },
  })
  async findById(@param.path.number('id') id: number): Promise<users> {
    return await this.usersRepository.findById(id);
  }

  @authenticate('BasicStrategy')
  @patch('/users/{id}', {
    responses: {
      '204': {
        description: 'Users PATCH success',
      },
    },
  })
  async updateById(
    @param.path.number('id') id: number,
    @requestBody() users: users,
  ): Promise<void> {
    await this.usersRepository.updateById(id, users);
  }

  @authenticate('BasicStrategy')
  @put('/users/{id}', {
    responses: {
      '204': {
        description: 'Users PUT success',
      },
    },
  })
  async replaceById(
    @param.path.number('id') id: number,
    @requestBody() users: users,
  ): Promise<void> {
    await this.usersRepository.replaceById(id, users);
  }

  @authenticate('BasicStrategy')
  @del('/users/{id}', {
    responses: {
      '204': {
        description: 'Users DELETE success',
      },
    },
  })
  async deleteById(@param.path.number('id') id: number): Promise<void> {
    await this.usersRepository.deleteById(id);
  }
}
