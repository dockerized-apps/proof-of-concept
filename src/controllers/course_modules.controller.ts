import {
  Count,
  CountSchema,
  Filter,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getFilterSchemaFor,
  getWhereSchemaFor,
  patch,
  put,
  del,
  requestBody,
} from '@loopback/rest';
import { course_modules } from '../models';
import { Course_ModulesRepository } from '../repositories';

export class CourseModulesController {
  constructor(
    @repository(Course_ModulesRepository)
    public courseModulesRepository: Course_ModulesRepository,
  ) { }

  @post('/course-modules', {
    responses: {
      '200': {
        description: 'CourseModules model instance',
        content: { 'application/json': { schema: { 'x-ts-type': course_modules } } },
      },
    },
  })
  async create(@requestBody() courseModules: course_modules): Promise<course_modules> {
    return await this.courseModulesRepository.create(courseModules);
  }

  @get('/course-modules/count', {
    responses: {
      '200': {
        description: 'CourseModules model count',
        content: { 'application/json': { schema: CountSchema } },
      },
    },
  })
  async count(
    @param.query.object('where', getWhereSchemaFor(course_modules)) where?: Where,
  ): Promise<Count> {
    return await this.courseModulesRepository.count(where);
  }

  @get('/course-modules', {
    responses: {
      '200': {
        description: 'Array of CourseModules model instances',
        content: {
          'application/json': {
            schema: { type: 'array', items: { 'x-ts-type': course_modules } },
          },
        },
      },
    },
  })
  async find(
    @param.query.object('filter', getFilterSchemaFor(course_modules)) filter?: Filter,
  ): Promise<course_modules[]> {
    return await this.courseModulesRepository.find(filter);
  }

  @patch('/course-modules', {
    responses: {
      '200': {
        description: 'CourseModules PATCH success count',
        content: { 'application/json': { schema: CountSchema } },
      },
    },
  })
  async updateAll(
    @requestBody() courseModules: course_modules,
    @param.query.object('where', getWhereSchemaFor(course_modules)) where?: Where,
  ): Promise<Count> {
    return await this.courseModulesRepository.updateAll(courseModules, where);
  }

  @get('/course-modules/{id}', {
    responses: {
      '200': {
        description: 'CourseModules model instance',
        content: { 'application/json': { schema: { 'x-ts-type': course_modules } } },
      },
    },
  })
  async findById(@param.path.number('id') id: number): Promise<course_modules> {
    return await this.courseModulesRepository.findById(id);
  }

  @patch('/course-modules/{id}', {
    responses: {
      '204': {
        description: 'CourseModules PATCH success',
      },
    },
  })
  async updateById(
    @param.path.number('id') id: number,
    @requestBody() courseModules: course_modules,
  ): Promise<void> {
    await this.courseModulesRepository.updateById(id, courseModules);
  }

  @put('/course-modules/{id}', {
    responses: {
      '204': {
        description: 'CourseModules PUT success',
      },
    },
  })
  async replaceById(
    @param.path.number('id') id: number,
    @requestBody() courseModules: course_modules,
  ): Promise<void> {
    await this.courseModulesRepository.replaceById(id, courseModules);
  }

  @del('/course-modules/{id}', {
    responses: {
      '204': {
        description: 'CourseModules DELETE success',
      },
    },
  })
  async deleteById(@param.path.number('id') id: number): Promise<void> {
    await this.courseModulesRepository.deleteById(id);
  }
}
