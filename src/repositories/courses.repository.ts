import { DefaultCrudRepository } from '@loopback/repository';
import { courses } from '../models';
import { MysqlDatasourceDataSource } from '../datasources';
import { inject } from '@loopback/core';

export class CoursesRepository extends DefaultCrudRepository<
  courses,
  typeof courses.prototype.id
  > {
  constructor(
    @inject('datasources.mysqlDatasource') dataSource: MysqlDatasourceDataSource,
  ) {
    super(courses, dataSource);
  }
}
