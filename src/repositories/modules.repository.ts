import { DefaultCrudRepository } from '@loopback/repository';
import { modules } from '../models';
import { MysqlDatasourceDataSource } from '../datasources';
import { inject } from '@loopback/core';

export class ModulesRepository extends DefaultCrudRepository<
  modules,
  typeof modules.prototype.id
  > {
  constructor(
    @inject('datasources.mysqlDatasource') dataSource: MysqlDatasourceDataSource,
  ) {
    super(modules, dataSource);
  }
}
