import {DefaultCrudRepository} from '@loopback/repository';
import {users} from '../models';
import {MysqlDatasourceDataSource} from '../datasources';
import {inject} from '@loopback/core';

export class UsersRepository extends DefaultCrudRepository<
  users,
  typeof users.prototype.id
> {
  constructor(
    @inject('datasources.mysqlDatasource')
    dataSource: MysqlDatasourceDataSource,
  ) {
    super(users, dataSource);
  }
}
