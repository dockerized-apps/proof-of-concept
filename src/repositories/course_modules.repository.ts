import { DefaultCrudRepository } from '@loopback/repository';
import { course_modules } from '../models';
import { MysqlDatasourceDataSource } from '../datasources';
import { inject } from '@loopback/core';

export class Course_ModulesRepository extends DefaultCrudRepository<
  course_modules,
  typeof course_modules.prototype.id
  > {
  constructor(
    @inject('datasources.mysqlDatasource') dataSource: MysqlDatasourceDataSource,
  ) {
    super(course_modules, dataSource);
  }
}
